Source: wcslib
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>,
           Phil Wyett <philip.wyett@kathenas.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               doxygen,
               flex,
               gfortran,
               giza-dev,
               libcfitsio-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/wcslib
Vcs-Git: https://salsa.debian.org/debian-astro-team/wcslib.git
Homepage: https://www.atnf.csiro.au/people/mcalabre/WCS/
Rules-Requires-Root: no

Package: wcslib-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libpgsbox8 (= ${binary:Version}),
         libwcs8 (= ${binary:Version}),
         ${misc:Depends}
Recommends: giza-dev
Suggests: wcslib-doc
Description: Header files and static library for wcslib and pgsbox
 WCSLIB is a C library, supplied with a full set of Fortran wrappers, that
 implements the "World Coordinate System" (WCS) standard in FITS (Flexible
 Image Transport System).
 .
 PGSBOX draws and labels a curvilinear coordinate grid.  The caller
 must provide a separate external function, NLFUNC, to define the
 non-linear coordinate transformation.
 .
 This package contains the static libraries and the C header files.

Package: libwcs8
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Implementation of the FITS WCS standard
 WCSLIB is a C library, supplied with a full set of Fortran wrappers, that
 implements the "World Coordinate System" (WCS) standard in FITS (Flexible
 Image Transport System).
 .
 The FITS data format is widely used within the international astronomical
 community, from the radio to gamma-ray regimes, for data interchange and
 archive, and also increasingly as an online format.
 .
 This package contains what you need to run programs that use this
 library.

Package: libpgsbox8
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Draw and label curvilinear coordinate grids with pgplot
 PGSBOX draws and labels a curvilinear coordinate grid.  The caller
 must provide a separate external function, NLFUNC, to define the
 non-linear coordinate transformation.

Package: wcslib-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Suggests: wcslib-dev
Description: API documentation for wcslib
 WCSLIB is a C library, supplied with a full set of Fortran wrappers, that
 implements the "World Coordinate System" (WCS) standard in FITS (Flexible
 Image Transport System).
 .
 This package contains the API documentation for WCSLIB.

Package: wcslib-tools
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Command line tools utilizing wcslib
 WCSLIB is a C library, supplied with a full set of Fortran wrappers, that
 implements the "World Coordinate System" (WCS) standard in FITS (Flexible
 Image Transport System).
 .
 This package contains the utility programs fitshdr, wcsware, HPXcvt,
 and wcsgrid that are included in wcslib.
